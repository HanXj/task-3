﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class Game{


    public static Game current;
    public Character knight;
    public Character rogue;
    public Character wizard;

    public Game()
    {
        knight = new Character();
        rogue = new Character();
        wizard = new Character();
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
